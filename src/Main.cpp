/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on February 27, 2014, 12:54 PM
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>

#ifdef	__cplusplus
extern "C" {
#endif
#include <signal.h>
#include <syslog.h>
#ifdef	__cplusplus
}
#endif    

#include "TurtlebotHttpServer.h"


using namespace std;

TurtlebotHttpServer *xmlrpc_server_inst = NULL;
int signal_to_exit = 0;

static void signal_handler(int status) {
    if (xmlrpc_server_inst != NULL) {
        syslog(LOG_NOTICE, "signal_handler(): stopping turtlebot_server.");
        xmlrpc_server_inst->stop_turtlebot_server();
        xmlrpc_server_inst = NULL;
        syslog(LOG_NOTICE, "signal_handler(): stopped.");
    }
    signal_to_exit = 1;

}

int main(int argc, char** argv) {
    int i, c;
    int dflag = 0;

    opterr = 0;
    while ((c = getopt(argc, argv, "dh")) != -1) {
        switch (c) {
            case 'd':
                dflag = 1;
                break;
            case 'h':
                printf("Usage: %s [-d ]\n", argv[0]);
                exit(0);
                break;
            default:
                break;
        }
    }
    if (dflag)
        if (daemon(1, 0) < 0) {
            syslog(LOG_NOTICE, "%s: Failed to daemonize process,aborted!\n", argv[0]);
            exit(EXIT_FAILURE);
        }
    openlog(argv[0], LOG_NOWAIT | LOG_PID, LOG_USER);

    struct sigaction saio;
    saio.sa_handler = signal_handler;
    saio.sa_flags = 0;
    saio.sa_restorer = NULL;
    sigaction(SIGINT, &saio, NULL);
    sigaction(SIGTERM, &saio, NULL);

    TurtlebotHttpServer turtleBot;
    xmlrpc_server_inst = &turtleBot;
    turtleBot.start_turtlebot_server();
    turtleBot.~TurtlebotHttpServer();
    closelog();
    return 0;

}

