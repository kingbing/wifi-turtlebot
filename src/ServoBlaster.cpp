/* 
 * File:   ServoBlaster.cpp
 * Author: user
 * 
 * Created on July 29, 2014, 2:15 PM
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "ServoBlaster.h" 

#define DEVFILE	"/dev/servoblaster"
#define MSLEEP(x) usleep(1000 * x) 
#define SERVO_MAX 250
#define SERVO_MIN 50
#define PWM(angle) ((int) (SERVO_MIN + (SERVO_MAX - SERVO_MIN) * angle / 180.0))

ServoBlaster::ServoBlaster() {
    fd = open(DEVFILE, O_RDWR | O_NONBLOCK);
    if (fd < 0) {
        syslog(LOG_NOTICE, "Failed to open  %s \n", DEVFILE);
        exit(EXIT_FAILURE);
    }
}

ServoBlaster::ServoBlaster(const ServoBlaster& orig) {
}

void ServoBlaster::runServo(int degrees[], int total_servos, int delay) {
    static const char *p1_header_pins[] = {
        "P1-7",
        "P1-11", "P1-12",
        "P1-13", "P1-15",
        "P1-16", "P1-18",
        "P1-22"
    };
    static char servo_cmd[64];
    for (int pin = 0; pin < total_servos; pin++) {
        sprintf(servo_cmd, "%s=%d\n", p1_header_pins[pin], PWM(degrees[pin]));
        write(fd, servo_cmd, strlen(servo_cmd));
    }
    MSLEEP(delay);
}

ServoBlaster::~ServoBlaster() {
    if (fd >= 0) close(fd);

}
